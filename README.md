# Homo regulans

> Luiz Cláudio Silveira Duarte

Obrigado por seu interesse! Esta é a página do repositório `git` para o livro *Homo regulans*.

Aqui você encontra o [arquivo em formato PDF](https://gitlab.com/impressoes/homoregulans/-/blob/main/Introducao.pdf) com a Introdução ao livro.

O livro pode ser adquirido na [Amazon](https://www.amazon.com.br/dp/B0BL23HR6T) ou na [Smashwords](https://www.smashwords.com/books/view/1176611), em formato EPUB; ou pode ser adquirido no [Buymeacoffee](https://www.buymeacoffee.com/quartelmestre/e/98459), em formato PDF.

Ficarei muito feliz em receber correções, críticas, sugestões e comentários ao livro. Para isso, por favor veja as instruções um pouco mais abaixo.

## Apresentação

Este é o primeiro volume da série *Impressões*. Como o nome indica, são livros escritos a partir de uma perspectiva impressionista, fruto de minhas percepções e reflexões sobre jogos e jogadores, ao longo de muitos anos.

*Homo regulans* funciona como uma introdução geral; embora seu tema geral seja o das relações entre pessoas e regras, esta discussão encontra-se principalmente na terceira parte do livro. As duas primeiras partes servem para estabelecer um conjunto de informações comuns: a primeira parte é um esboço da longa história dos jogos, e a segunda traz discussões sobre os elementos que compõem um jogo.

Neste livro, assim como em outros da série, tenho especial interesse nas pessoas e nas relações que estabelecem -- entre si, e com os jogos. Este foco nas pessoas e nas suas relações permite fazer uma brincadeira conceitual: este não é propriamente um livro sobre jogos, e sim sobre a ecologia dos jogadores.

## Sua participação

Para poder participar da conversa sobre o *Homo regulans*, você precisa [criar uma conta](https://gitlab.com/users/sign_up) aqui no GitLab. Este é um serviço gratuito, usado especialmente por criadores de *software*, mas que pode ser muito útil para escritores também -- o desenvolvimento do *Homo regulans* aconteceu aqui.

Depois de criar sua conta, você pode usar a funcionalidade de *Issues* para enviar sua participação. Veja o menu da esquerda (em um celular, ele se abre com um toque no botão superior, à esquerda), ou clique diretamente [aqui](https://gitlab.com/impressoes/homoregulans/-/issues).

A tela que se abre é a lista de *Issues* já registradas. Você pode ler e responder a qualquer uma delas, ou pode também criar sua própria *Issue*; basta usar o botão *New issue*.
